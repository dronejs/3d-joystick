import Engine from './classes/Engine';
import Control from './classes/Control';

const pwmRange = {
  min: 1142,
  max: 1836
};

const engines = {
  front: {
    left: new Engine({
      gpio: 17,
      pwmRange: pwmRange
    }),
    right: new Engine({
      gpio: 18,
      pwmRange: pwmRange
    })
  },
  rear: {
    left: new Engine({
      gpio: 26,
      pwmRange: pwmRange
    }),
    right: new Engine({
      gpio: 21,
      pwmRange: pwmRange
    })
  }
};


new Control(engines);









