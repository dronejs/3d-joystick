import SocketConnection from '../classes/SocketConnection';

let socket = null;

if (!socket) {
  socket = new SocketConnection();
}

export default socket;