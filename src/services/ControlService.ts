import Control from '../classes/Control';

let control: Control = null;

if (!control) {
  control = new Control();
}

export default control;