import * as express from "express";
import * as http from "http";
import * as socketIo from "socket.io";

import * as chalk from 'chalk';


export default class SocketConnection {

  public PORT = 8080;
  public app: any;
  private server: any;
  private io: any;
  private port: number;
  private listeners: any[];

  constructor(PORT: number = 8080) {

    this.PORT = PORT;
    this.app = express();
    this.server = http.createServer(this.app);
    this.port = process.env.PORT || this.PORT;
    this.io = socketIo(this.server);
    this.listeners = [];
    this.listen();
  }

  addListener(command: string, cb) {

    if (!this.listeners[command]) {
      this.listeners[command] = [];
    }

    this.listeners[command].push(cb);

  }

  invokeCallbacks(command: string, payload: any) {



    if (!this.listeners[command]) {
      return;
    }

    this.listeners[command].forEach(cb => {
      cb(payload);
    });

  }

  postMessage(command, payload) {
    this.io.emit(command, payload);
  }


  private listen(): void {
    this.server.listen(this.port, () => {
      console.log(chalk.bgGreen.black(`Running server on port ${this.port}`));
    });

    this.io.on('connect', (socket: any) => {
      console.log('Connected client on port %s.', this.port);
      socket.on('command', data => {
        this.invokeCallbacks(data.command, data.payload);
      });

      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  }

}