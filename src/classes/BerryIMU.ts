import { connect } from 'socket.io-client'
import { Observable } from 'rx';
import { IMUReadings } from '../utils/IMUReadings';

let hrend;
let hrstart = process.hrtime();

export default class BerryIMU {

  connection;
  feed: Observable<any>;

  constructor() {
    this.connection = connect('http://localhost:8090');

    this.feed = Observable.create(observer => {

      this.connection.on('tilt', (data: IMUReadings) => {
        hrend = process.hrtime(hrstart);
        observer.onNext({
          x: data.x,
          y: data.y,
          heading: data.heading,
          readTime: hrend[1]/1000000
        });
        hrstart = process.hrtime();
      })
    })
  }
}