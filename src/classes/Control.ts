import socket from '../services/SocketService';
import { IMUReadings } from '../utils/IMUReadings';
import { measureTilt } from '../utils/berryimu/Orientation';
const NanoTimer = require('nanotimer');

let PID = require('node-pid-controller');

let pid = new PID({
  k_p: 4.5,
  k_i: 1,
  k_d: 5,
  dt: 0.5
});

pid.setTarget(0);

export default class Control {

  engines: object;

  correction;
  throttle: number = 0;

  targetYaw: number = null;

  IMUReadings: IMUReadings = {
    roll: 0,
    pitch: 0,
    yaw: 0
  };

  constructor(engines) {

    this.engines = engines;

    this.startIMU();
    this.startTelemetry();


    setTimeout(() => {

        this.targetYaw = this.IMUReadings.yaw;
        console.log('target YAW', this.targetYaw);
        pid.setTarget(this.targetYaw);

      this.startPID();

    }, 1000);


  }

  startIMU() {
    const timer = new NanoTimer();
    timer.setInterval(() => {
      this.IMUReadings = measureTilt();
    }, '', '0n');
  }

  startPID() {

    const timer = new NanoTimer();

    timer.setInterval(() => {
      this.calculateCorrection();
    }, '', '5m');

  }

  startTelemetry() {
    setInterval(() => {
      socket.postMessage('tilt', this.IMUReadings);
      socket.postMessage('correction', this.correction);
    }, 100);
  }

  calculateCorrection() {

    this.correction = pid.update(this.IMUReadings.yaw);
  }


}