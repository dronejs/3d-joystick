import { Gpio } from 'pigpio';
import { EngineSettings } from '../utils/EngineSettings';

export default class Engine  {


  connection: Gpio;
  settings: EngineSettings;
  throttle: number = 0;
  currentPulse: number;

  engineInterval;


  constructor(settings: EngineSettings) {

    this.settings = settings;
    this.connection = new Gpio(this.settings.gpio, {mode: Gpio.OUTPUT});

  }

  /**
   *
   * @param oldMax
   * @param oldMin
   * @param newMax
   * @param newMin
   * @param currentValue
   * @returns {number}
   */
  scaleRange(oldMax: number, oldMin: number, newMax: number, newMin: number, currentValue: number): number {
    const OldRange = oldMax - oldMin;
    const NewRange = newMax - newMin;
    return (((currentValue) * NewRange) / OldRange) + newMin;
  }

  getScaledPulseWidth(currentThrottle: number) {
    return Math.ceil(this.scaleRange(100, 0, this.settings.pwmRange.max, this.settings.pwmRange.min, currentThrottle));
  }

  disarmMotor() {
    clearInterval(this.engineInterval);
  }

  armMotor() {
    this.engineInterval = setInterval(() => {
      this.connection.servoWrite(Math.floor(this.currentPulse));
    }, 10);

  }

  calculateThrottle() {
    this.currentPulse = this.getScaledPulseWidth(this.throttle);
    return this.currentPulse;
  }

  getStatus() {
    return {
      settings: this.settings,
      currentPulse: this.currentPulse
    }
  }

}