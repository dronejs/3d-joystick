export type IMUReadings = {
  roll: number;
  pitch: number;
  yaw: number;
}