export type EngineSettings  = {
  gpio: number;
  pwmRange: PwmRange;
}

export type PwmRange = {
  min: number;
  max: number;
}