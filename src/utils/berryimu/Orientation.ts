import { openSync } from 'i2c-bus';
const i2c1 = openSync(1);

import { Accelometer } from './sensors/accelometer';
import { Gyroscope } from './sensors/gyroscope';
import { Magnetometr } from './sensors/magnetometr';
import { Barometer } from './sensors/barometer';

import { kalmanFilterX, kalmanFilterY } from './kalmanFilter';

const accelerometer = new Accelometer(i2c1);
const magnetometer = new Magnetometr(i2c1);
const gyroscope = new Gyroscope(i2c1);
const barometer = new Barometer(i2c1);


const IMU_upside_down = 0;

const RAD_TO_DEG = 57.29578;
const M_PI = 3.14159265358979323846;
const G_GAIN = 0.070;
const AA =  0.40;
const MAG_LPF_FACTOR = 0.4;
const ACC_LPF_FACTOR = 0.4;
const ACC_MEDIANTABLESIZE = 9;
const MAG_MEDIANTABLESIZE = 9;



let gyroXangle = 0.0;
let gyroYangle = 0.0;
let gyroZangle = 0.0;
let CFangleX = 0.0;
let CFangleY = 0.0;
let CFangleXFiltered = 0.0;
let CFangleYFiltered = 0.0;
let kalmanX = 0.0;
let kalmanY = 0.0;
let oldXMagRawValue = 0;
let oldYMagRawValue = 0;
let oldZMagRawValue = 0;
let oldXAccRawValue = 0;
let oldYAccRawValue = 0;
let oldZAccRawValue = 0;

let a = Date.now();

let acc_medianTable1X = new Array(ACC_MEDIANTABLESIZE).fill(1);
let acc_medianTable1Y = new Array(ACC_MEDIANTABLESIZE).fill(1);
let acc_medianTable1Z = new Array(ACC_MEDIANTABLESIZE).fill(1);
let acc_medianTable2X = new Array(ACC_MEDIANTABLESIZE).fill(1);
let acc_medianTable2Y = new Array(ACC_MEDIANTABLESIZE).fill(1);
let acc_medianTable2Z = new Array(ACC_MEDIANTABLESIZE).fill(1);

let mag_medianTable1X = new Array(MAG_MEDIANTABLESIZE).fill(1);
let mag_medianTable1Y = new Array(MAG_MEDIANTABLESIZE).fill(1);
let mag_medianTable1Z = new Array(MAG_MEDIANTABLESIZE).fill(1);
let mag_medianTable2X = new Array(MAG_MEDIANTABLESIZE).fill(1);
let mag_medianTable2Y = new Array(MAG_MEDIANTABLESIZE).fill(1);
let mag_medianTable2Z = new Array(MAG_MEDIANTABLESIZE).fill(1);

export function measureTilt() {
    let ACCx = accelerometer.readX();
    let ACCy = accelerometer.readY();
    let ACCz = accelerometer.readZ();

    let GYRx = gyroscope.readX();
    let GYRy = gyroscope.readY();
    let GYRz = gyroscope.readZ();

    let MAGx = magnetometer.readX();
    let MAGy = magnetometer.readY();
    let MAGz = magnetometer.readZ();


    let LP = Date.now() - a;
    a = Date.now();

    MAGx =  MAGx  * MAG_LPF_FACTOR + oldXMagRawValue*(1 - MAG_LPF_FACTOR);
    MAGy =  MAGy  * MAG_LPF_FACTOR + oldYMagRawValue*(1 - MAG_LPF_FACTOR);
    MAGz =  MAGz  * MAG_LPF_FACTOR + oldZMagRawValue*(1 - MAG_LPF_FACTOR);
    ACCx =  ACCx  * ACC_LPF_FACTOR + oldXAccRawValue*(1 - ACC_LPF_FACTOR);
    ACCy =  ACCy  * ACC_LPF_FACTOR + oldYAccRawValue*(1 - ACC_LPF_FACTOR);
    ACCz =  ACCz  * ACC_LPF_FACTOR + oldZAccRawValue*(1 - ACC_LPF_FACTOR);


    oldXMagRawValue = MAGx;
    oldYMagRawValue = MAGy;
    oldZMagRawValue = MAGz;
    oldXAccRawValue = ACCx;
    oldYAccRawValue = ACCy;
    oldZAccRawValue = ACCz;

    for(let i = ACC_MEDIANTABLESIZE-1; i > 0; i--) {
        acc_medianTable1X[i] = acc_medianTable1X[i-1];
        acc_medianTable1Y[i] = acc_medianTable1Y[i-1];
        acc_medianTable1Z[i] = acc_medianTable1Z[i-1];
    }


    acc_medianTable1X[0] = ACCx;
    acc_medianTable1Y[0] = ACCy;
    acc_medianTable1Z[0] = ACCz;


    acc_medianTable2X = [...acc_medianTable1X];
    acc_medianTable2Y = [...acc_medianTable1Y];
    acc_medianTable2Z = [...acc_medianTable1Z];


    acc_medianTable2X.sort();
    acc_medianTable2Y.sort();
    acc_medianTable2Z.sort();


    ACCx = acc_medianTable2X[Math.ceil(ACC_MEDIANTABLESIZE/2)];
    ACCy = acc_medianTable2Y[Math.ceil(ACC_MEDIANTABLESIZE/2)];
    ACCz = acc_medianTable2Z[Math.ceil(ACC_MEDIANTABLESIZE/2)];





    for(let i = MAG_MEDIANTABLESIZE-1; i > 0; i--) {
        mag_medianTable1X[i] = mag_medianTable1X[i - 1];
        mag_medianTable1Y[i] = mag_medianTable1Y[i - 1];
        mag_medianTable1Z[i] = mag_medianTable1Z[i - 1];
    }


    mag_medianTable1X[0] = MAGx;
    mag_medianTable1Y[0] = MAGy;
    mag_medianTable1Z[0] = MAGz;


    mag_medianTable2X = [...mag_medianTable1X];
    mag_medianTable2Y = [...mag_medianTable1Y];
    mag_medianTable2Z = [...mag_medianTable1Z];



    mag_medianTable2X.sort();
    mag_medianTable2Y.sort();
    mag_medianTable2Z.sort();


    MAGx = mag_medianTable2X[Math.ceil(MAG_MEDIANTABLESIZE/2)];
    MAGy = mag_medianTable2Y[Math.ceil(MAG_MEDIANTABLESIZE/2)];
    MAGz = mag_medianTable2Z[Math.ceil(MAG_MEDIANTABLESIZE/2)];




    let rate_gyr_x =  GYRx * G_GAIN;
    let rate_gyr_y =  GYRy * G_GAIN;
    let rate_gyr_z =  GYRz * G_GAIN;



    gyroXangle += rate_gyr_x * LP;
    gyroYangle += rate_gyr_y * LP;
    gyroZangle += rate_gyr_z * LP;

    let AccXangle =  (Math.atan2(ACCy,ACCz)+M_PI)*RAD_TO_DEG;
    let AccYangle =  (Math.atan2(ACCz,ACCx)+M_PI)*RAD_TO_DEG;


    if (!!IMU_upside_down) {
        if (AccXangle >180) {
            AccXangle -= 360.0;
            AccYangle-=90;
        }

        if (AccYangle >180) {
            AccYangle -= 360.0;
        }

    } else {
        AccXangle -= 180.0;
        if (AccYangle > 90) {
            AccYangle -= 270.0;
        } else {
            AccYangle += 90.0;
        }

    }


    CFangleX=AA*(CFangleX+rate_gyr_x*LP) +(1 - AA) * AccXangle;
    CFangleY=AA*(CFangleY+rate_gyr_y*LP) +(1 - AA) * AccYangle;



    kalmanY = kalmanFilterY(AccYangle, rate_gyr_y,LP);
    kalmanX = kalmanFilterX(AccXangle, rate_gyr_x,LP);



    if (!!IMU_upside_down) {
        MAGy = -MAGy;
    }



    let heading = 180 * Math.atan2(MAGy,MAGx)/M_PI;


    if (heading < 0) {
        heading += 360;
    }

    let accXnorm = ACCx/Math.sqrt(ACCx * ACCx + ACCy * ACCy + ACCz * ACCz);
    let accYnorm = ACCy/Math.sqrt(ACCx * ACCx + ACCy * ACCy + ACCz * ACCz);


    let pitch;
    let roll;

    if (!!IMU_upside_down) {
        accXnorm = -accXnorm;
        accYnorm = -accYnorm;
        pitch = Math.asin(accXnorm);
        roll = Math.asin(accYnorm/Math.cos(pitch));
    } else {
        pitch = Math.asin(accXnorm);
        roll = -Math.asin(accYnorm/Math.cos(pitch));
    }


    let magXcomp = MAGx*Math.cos(pitch)+MAGz*Math.sin(pitch);
    let magYcomp = MAGx*Math.sin(roll)*Math.sin(pitch)+MAGy*Math.cos(roll)-MAGz*Math.sin(roll)*Math.cos(pitch);


    let tiltCompensatedHeading = 180 * Math.atan2(magYcomp,magXcomp)/M_PI;


    if (tiltCompensatedHeading < 0) {
        tiltCompensatedHeading += 360;
    }

    return {
        roll: kalmanX,
        pitch: kalmanY,
        yaw: heading
    };

}

export function measurePresure() {
    return barometer.measure()
}

