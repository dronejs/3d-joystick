import { BMP180 } from '../BMP180';
import { I2cBus } from 'i2c-bus';


function s_short(v) {
    return (((v[0] << 8) | v[1]) << 16) >> 16;
}

function get_short(data, index) {
    return s_short([data[index], data[index + 1]]);
}

function get_ushort(data, index) {
    return (data[index] << 8) + data[index + 1];
}

export class Barometer {

    private bus: I2cBus;
    private address: number;
    private oversampling = 3;

    constructor(bus: I2cBus) {
        this.bus = bus;
        this.address = BMP180.BAR_ADDRESS;
    }

    private write(register,value) {
        return this.bus.writeByteSync(this.address, register, value);
    }

    private read(address, command, buff) {
        return this.bus.readI2cBlockSync(this.address, address, command, buff);
    }

    /**
     *
     * @returns {Promise<{temperature: number, presure: number}>}
     */
    measure() {

        const buff = Buffer.allocUnsafe(32);
        
        return new Promise(resolve => {

            this.read(BMP180.AC1, 22, buff);

            let ac1 = get_short(buff, 0);
            let ac2 = get_short(buff, 2);
            let ac3 = get_short(buff, 4);
            let ac4 = get_ushort(buff, 6);
            let ac5 = get_ushort(buff, 8);
            let ac6 = get_ushort(buff, 10);
            let b1 = get_short(buff, 12);
            let b2 = get_short(buff, 14);
            let mb = get_short(buff, 16);
            let mc = get_short(buff, 18);
            let md = get_short(buff, 20);

            this.write(0xF4, 0x2E);

            setTimeout(() => {

                this.read(0xF6, 2, buff);

                let ut = (buff[0] << 8) + buff[1];

                this.write(0xF4, 0x34 + (this.oversampling << 6));

                setTimeout(() => {

                    this.read(0xF6, 3, buff);

                    let msb = buff[0];
                    let lsb = buff[1];
                    let xsb = buff[2];

                    let up = ((msb << 16) + (lsb << 8) + xsb) >> (8 - this.oversampling);
                    let x1 = ((ut - ac6) * ac5) >> 15;
                    let x2 = (mc << 11) / (x1 + md);
                    let b5 = x1 + x2;
                    let t = (b5 + 8) >> 4;

                    let b6 = b5 - 4000;
                    let b62 = b6 * b6 >> 12;
                    x1 = (b2 * b62) >> 12;
                    x2 = ac2 * b6 >> 11;
                    let x3 = x1 + x2;
                    let b3 = (((ac1 * 4 + x3) << this.oversampling) + 2) >> 2;

                    x1 = ac3 * b6 >> 13;
                    x2 = (b1 * b62) >> 16;
                    x3 = ((x1 + x2) + 2) >> 2;
                    let b4 = (ac4 * (x3 + 32768)) >> 15;
                    let b7 = (up - b3) * (50000 >> this.oversampling);

                    let p = (b7 * 2) / b4;

                    x1 = (p >> 8) * (p >> 8);
                    x1 = (x1 * 3038) >> 16;
                    x2 = (-7357 * p) >> 16;
                    p = p + ((x1 + x2 + 3791) >> 4);

                    resolve({
                        temperature: t/10.0,
                        pressure: p/100.0
                    })

                }, 40);

            }, 10);
            
        });




    }


}



