import { LSM9DS0 } from '../LSM9DS0';
import { I2cBus } from 'i2c-bus';


export class Magnetometr {

    bus: I2cBus;
    private address: number;

    constructor(bus: I2cBus) {
        this.bus = bus;
        this.address = LSM9DS0.MAG_ADDRESS;

        this.write(LSM9DS0.CTRL_REG5_XM, 0b11110000);
        this.write(LSM9DS0.CTRL_REG6_XM, 0b01100000);
        this.write(LSM9DS0.CTRL_REG7_XM, 0b00000000);

    }


    write(register,value) {
        return this.bus.writeByteSync(this.address, register, value);
    }

    private read(magL, magH) {


        const mag_l = this.bus.readByteSync(this.address, magL);
        const mag_h = this.bus.readByteSync(this.address, magH);
        const mag_combined = (mag_l | mag_h <<8);

        return mag_combined < 32768 ? mag_combined : mag_combined - 65536;
    }

    readX() {
        return this.read(LSM9DS0.OUT_X_L_M, LSM9DS0.OUT_X_H_M);
    }

    readY() {
        return this.read(LSM9DS0.OUT_Y_L_M, LSM9DS0.OUT_Y_H_M);
    }

    readZ() {
        return this.read(LSM9DS0.OUT_Z_L_M, LSM9DS0.OUT_Z_H_M);
    }


}


