import { LSM9DS0 } from '../LSM9DS0';
import { I2cBus } from 'i2c-bus';


export class Gyroscope {

    bus: I2cBus;
    private address: number;

    constructor(bus: I2cBus) {
        this.bus = bus;
        this.address = LSM9DS0.GYR_ADDRESS;


        this.write(LSM9DS0.CTRL_REG1_G, 0b00001111);
        this.write(LSM9DS0.CTRL_REG4_G, 0b00110000);

    }


    write(register,value) {
        return this.bus.writeByteSync(this.address, register, value);
    }

    private read(gyroL, gyroH) {


        const gyro_l = this.bus.readByteSync(this.address, gyroL);
        const gyro_h = this.bus.readByteSync(this.address, gyroH);
        const gyro_combined = (gyro_l | gyro_h <<8);

        return gyro_combined < 32768 ? gyro_combined : gyro_combined - 65536;
    }

    readX() {
        return this.read(LSM9DS0.OUT_X_L_G, LSM9DS0.OUT_X_H_G);
    }

    readY() {
        return this.read(LSM9DS0.OUT_Y_L_G, LSM9DS0.OUT_Y_H_G);
    }

    readZ() {
        return this.read(LSM9DS0.OUT_Z_L_G, LSM9DS0.OUT_Z_H_G);
    }


}


